#!/bin/bash
config=$1
if [ -z $config ]; then
        echo "Usage: do_predict.sh <configfile>"
        exit
fi

config=`realpath $config`
if [ ! -e $config ]; then
        echo "No such file $config"
        exit
fi

source $config
echo $factor

# Create output directory in tmpdir
#if [ -z $TMPDIR ]; then
#        TMPDIR=`mktemp -d`
#fi
#echo "Using tmpdir $TMPDIR"

#cd $TMPDIR
#if [ ! -e $FACTOR ]; then
#        mkdir $FACTOR;
#fi

#cp $SPLIT_DIR/x* $TMPDIR
#icp $SPLIT_DIR/* $TMPDIR
#cp $BASE/$FACTOR/*.pwm* $TMPDIR/$FACTOR/
#cp $BASE/$FACTOR/features.txt* $TMPDIR/$FACTOR/
#cp $BASE/$FACTOR/threshold.txt* $TMPDIR/$FACTOR/
#cp $BASE/data/norm_factor.txt $TMPDIR
#cp $BASE/$FACTOR/model/ $FACTOR
#gunzip $TMPDIR/$FACTOR/*gz
#
#for fname in $DREAMBASE/DNASE/fold_coverage_wiggles/DNASE.*.bigwig; do
#	cp $fname $TMPDIR/`basename $fname .bigwig`.bw
#done

#gunzip $FACTOR/*.gz

#for i in $TMPDIR/xa[a-z]; do
for i in $SPLIT_DIR/xaw; do
    out=$OUTDIR/`basename $i`.$PREDICT_CELL.proba.bed
    python src/gimmebinding.py predict $config $i $out
done

#cp $FACTOR/*.tab $BASE/$FACTOR/

#rm $TMPDIR/$FACTOR/*
#rm -r $TMPDIR/*

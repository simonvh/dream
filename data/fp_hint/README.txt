This directory contains predicted footprints.

To (re)create these footprints:
1) Download HINT (rgt-hint) here: http://www.regulatory-genomics.org/hint/introduction/
2) Make sure rgt-hint is in the path
3) Adapt footprint.sh with correct paths etc., and run once for every celltype

#!/bin/bash
#SBATCH -t 12:00:00
#SBATCH -N 1

# This script was created for running on a SLURM cluster
# Currently not adapted to be general
# ALL paths need to be adapted
# Run/submit once for each cell type

set -e

cell=A549
BASE=/home/simonvh/dream/

cd $TMPDIR
cp /scratch-shared/gimmebinding/dnase_bam/DNASE.$cell*.bam* .
cut -f1-3 $BASE/DNASE/peaks/idr/DNASE.$cell.conservative.narrowPeak > DNASE.$cell.conservative.narrowPeak.bed
for bamfile in *bam; do
    exp=`basename $bamfile .bam`
    cat $BASE/fp_hint/matrix.txt | sed "s/CELL/$cell/g" | sed "s/EXP/$exp/g" > $exp.matrix.txt
    echo "***"
    cat $exp.matrix.txt
    echo "***"
    rgt-hint --output-location ./ $exp.matrix.txt &
done
wait
rm DNASE.$cell*.bam*
mv * $BASE/fp_hint/out


#!/bin/bash
#SBATCH -t 24:00:00
#SBATCH -N 1

# SLURM batch script, paths need to be adapted

INFILE=$HOME/dream/ChIPseq/peaks/conservative/ChIPseq.GM12878.TCF12.conservative.train.narrowPeak.gz
cp $INFILE $TMPDIR
NAME=`basename $INFILE .gz`.motifs
cd $TMPDIR
zcat `basename $INFILE` | shuf | head -n 5000 > input.bed
gimme motifs input.bed -g hg19 -n $NAME -a xl -b gc,random -k
cp -r $NAME $HOME/dream/gimme.motifs.out/with_intermediate/

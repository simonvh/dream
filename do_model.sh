#!/bin/bash

config=`realpath $1`

if [ -z $config ]; then 
	echo "Usage: run_all.sh <configfile>"
	exit
elif [ ! -e $config ]; then
	echo "No such file $config"
	exit
fi

### Read configuration ###
source $config
echo $FACTOR

# Get features
python src/gimmebinding.py train $config 

# Run model
python $BASE/src/learn_model.py $FACTOR

# Copy stuff to output directory
#gzip $FACTOR/*
#touch $FACTOR/done.txt
#cp -r $FACTOR $BASE

# Remove files in temporary directory
#rm $TMPDIR/$FACTOR/*
#rmdir $TMPDIR/$FACTOR
#rm $TMPDIR/*
#rmdir $TMPDIR

import click
import os
import sys
import tempfile

sys.path.append("src")
from utils import read_config, setup_logging
from main import make_test_features,make_train_features
from model import predict_proba

class Config(object):
    def __init__(self, config, tmpdir, outdir, logger):
        self.config = config
        self.tmpdir = tmpdir
        self.outdir = outdir
        self.logger = logger

_global_params = [
    click.argument("config")
]

def global_params(func):
    for option in reversed(_global_params):
        func = option(func)
    return func


def _init(config):
    logger = setup_logging()

    conf = read_config(config)
    factor = conf["FACTOR"]
    tmpdir = tempfile.mkdtemp(prefix=factor)
    outdir = conf["OUTDIR"]
    logger.info("using tmpdir: %s", tmpdir)
    logger.info("using outdir: %s", outdir)

    for d in [tmpdir, outdir]:
        if not os.path.exists(d):
            logger.debug("creating directory %s", d)
            try:
                os.makedirs(d)
            except:
                pass

    return Config(conf, tmpdir, outdir, logger)


@click.group()
def cli():
    pass

@click.command()
@global_params
def train(config):
    config = _init(config)
    make_train_features(config.config, config.tmpdir, config.outdir)

@click.command()
@global_params
@click.argument("infile")
@click.argument("outfile")
@click.option("-f", "--features", help="file with features")
def predict(config, infile, outfile, features):
    config = _init(config)
    logger = config.logger    
    
    logger.info("getting features")
    df = make_test_features(
            infile, 
            config.config, 
            config.tmpdir, 
            config.outdir,
            features)
    fname = os.path.join(config.tmpdir,
                os.path.basename(infile) + ".features.txt.gz")
    df.to_csv(fname, compression="gzip", sep="\t")
    
    factor = config.config["FACTOR"]
    
    logger.info("running model")
    df_prob = predict_proba(factor, fname)
    logger.info("saving output")
    df_prob["loc"] = df_prob.index.to_series().str.split(' ', expand=True)[0]
    df_prob[["chrom","start", "end"]] = df_prob["loc"].str.split(r'[\:\-]', expand=True)
    df_prob[["chrom", "start", "end", "proba"]].to_csv(
            outfile, sep="\t", index=False, header=False)

cli.add_command(train)
cli.add_command(predict)

if __name__ == "__main__":
    cli()

from tempfile import NamedTemporaryFile,mkdtemp
from gimmemotifs.motif import read_motifs
from gimmemotifs.fasta import Fasta
import subprocess as sp
import os
import re
import sys
from functools import partial
from multiprocessing import Pool
import numpy as np
from itertools import combinations, product

DWT_HOME = "/home/simon/git/DWT-toolbox"

dwt_fit = "python {}/Source/fitting_DWT_model.py".format(DWT_HOME)
dwt_pred = "python {}/Source/DWT_TFBS_prediction.py".format(DWT_HOME)

FA_P = re.compile(">?(\w+):(\d+)-(\d+)")

def make_parameter_file(output, background=None):
    if not background:
        background = {"A":0.25, "C":0.25, "G":0.25, "T":0.25}

    param_file = NamedTemporaryFile(delete=False)
    param_file.write('\n'.join([
            "#background",
            "A\t%0.4f" % background["A"],
            "C\t%0.4f" % background["C"],
            "G\t%0.4f" % background["G"],
            "T\t%0.4f" % background["T"],
            "",
            "#output_file",
            output,
        ]))

    param_file.flush()

    return param_file.name
    
def background_model(infile):
    """
    As input, this method takes a filename which contains the sequences in FASTA format
    and returns the sequences' base frequencies in a form of a dictionary.
    """
    if type("") == type(infile):
        infile = Fasta(infile)
    
    test = re.compile ("^>", re.IGNORECASE)
    reverse = {'A':'T', 'T':'A', 'C':'G', 'G':'C'}
    freq = {'A':0, 'C':0, 'G':0, 'T':0}
    for seq in infile.seqs:
        for b in seq.upper():
            try:
                freq[b] = freq[b] + 1
                freq[reverse[b]] = freq[reverse[b]] + 1
            except KeyError:
                continue
    normalization = float(sum(freq.values()))
    
    if normalization > 50:
        for b in freq.keys():
            freq[b] = freq[b]/normalization
        return freq
    else:
        return {'A':0.25, 'C':0.25, 'G':0.25, 'T':0.25}

def parse_line(vals, score_col=6):
    #['>chr10:977687-977887', '191', '199', '+', 'HepG2.CREB1.GimmeMotifs_1', 'ACATCGCC', '-12.9606']
    chrom = vals[0]
    mstart = int(vals[1])
    mend = int(vals[2])
    strand = vals[3]
    name = vals[4]
    score = float(vals[score_col])
        
    m = FA_P.search(chrom)
    if m:
        chrom = m.group(1)
        start = int(m.group(2))
        mstart = mstart + start 
        mend = mend + start
        
    return chrom, mstart, mend, name, score, strand


def scan_dwt_fast(infile, motif, scores_only=False):
    background = background_model(infile)
    param_file = make_parameter_file("/dev/stdout", background)
    fa = motevo_fa(infile)
    seqs = [">" + seq for seq in Fasta(fa).ids]
    
    prog = "{}/Source/DWT_TFBS_prediction".format(DWT_HOME)

    cmd = " ".join([prog, motif, fa, param_file])
    p = sp.Popen(cmd, shell = True, stdout=sp.PIPE, stderr=sp.PIPE)

    # Filter out one match with highest score (post prob)
    max_score = {}
    match = {}
    for line in p.stdout:
        vals = line.strip().split("\t")
        #print line
        score = float(vals[6])
        if score >= max_score.get(vals[0], 0):
            max_score[vals[0]] = score
            if scores_only:
                match[vals[0]] = score
            else:
                match[vals[0]] = vals

    motif_name = os.path.basename(motif).replace(".dwt", "")
    # Yield results
    p = re.compile(">?(\w+):(\d+)-(\d+)")
    for seq in seqs:
        if scores_only:
            yield match.get(seq, -100)
        else:
            vals = match.get(seq, [seq, 0, 0, "+", motif_name, "", -100, 0])
            chrom = seq
            mstart = int(vals[1])
            mend = int(vals[2])
            strand = vals[3]
            name = vals[4]
            score = float(vals[6])
            
            m = p.search(seq)
            if m:
                chrom = m.group(1)
                start = int(m.group(2))
                mstart = mstart + start 
                mend = mend + start
            #print chrom, mstart, mend, name, score, strand 
            yield chrom, mstart, mend, name, score, strand
    
def motevo_fa(fa):
    """
    Convert a FASTA file to a MotEvo FASTA file

    * >> for the sequence header
    * sequence all on one line
    
    Parameters
    ----------
    
    fa : str or Fasta object
        inputfile in FASTA format or Fasta object

    Returns
    -------

    str
        name of a MotEvo-formatted FASTA file, which will be a tempfile
    """
    
    if type("") == type(fa):
        fa = Fasta(fa)

    tmpfa = NamedTemporaryFile(delete=False)
    for name,seq in fa.items():
        name = name.replace(" ", "_")
        
        if not name.startswith(">"):
            name = ">" + name
        
        seq = seq.upper()
        
        tmpfa.write(">{}\n{}\n".format(name, seq))
    tmpfa.flush()
    
    return tmpfa.name

def predict_dwt(motif, infile, outdir):
    """
    Fit a DWT model for a motif using a FASTA inputfile
    
    Parameters
    ----------
    infile : str
        inputfile in FASTA format

    pwmfile : str
        pwmfile with one motif with positional frequences
    """

    outfile = os.path.join(outdir, motif.id + ".dwt")
    # Temporary FASTA file, as the FASTA headers need to be different
    tmpfa = motevo_fa(infile)
    
    # Convert pwm to JASPAR-like MotEvo format
    tmpmotif = NamedTemporaryFile(delete=False)
    tmpmotif.write(motif.to_motevo())
    tmpmotif.flush()

    # tmp output directory
    outdir = mkdtemp()
    
    cmd = "{} -w {} -f {} -o {} -t {}"
    p = sp.Popen(cmd.format(dwt_fit, tmpmotif.name, tmpfa, outdir, motif.id), 
            shell=True,
            stdout=sp.PIPE, stderr=sp.PIPE)
    
    p.communicate() 
    dwt_out = os.path.join(outdir, motif.id + ".dwt")

    with open(outfile, "w") as f:
        f.write(open(dwt_out).read())


def pwmfile_to_dwt(pwmfile, infile, outdir="./"):
    if not os.path.exists(outdir):
        os.makedirs(outdir)
   
    f = partial(predict_dwt, infile=infile, outdir=outdir)

    pool = Pool()
    pool.map(f, read_motifs(open(pwmfile)))
    pool.close()
    pool.join()

def scan_dwt_list(infile, dwtfile, score, scores_only):
    if score == "score":
        return list(scan_dwt_fast(infile, dwtfile, scores_only))
    elif score == "prob":
        return list(scan_dwt(infile, dwtfile))

def scan_fa_with_dwts(infile, dwts, score="score", scores_only=True, fa_chunks=8):
    #print infile
    #print dwts
    pool = Pool(processes=8)
    jobs = []
    if type("") == type(infile):
        fa = Fasta(infile)
    else:
        fa = infile
    
    n = (len(fa) + fa_chunks - 1) / fa_chunks
    for dwt in dwts:
        for i in range(0, len(fa), n):
            #for row in scan_dwt_list(infile, dwt, score):
            #    yield row
            j = pool.apply_async(scan_dwt_list, 
                    (fa[i:i+n], dwt, score, scores_only))
            jobs.append(j)
    
    for j in jobs:
        for row in j.get():
            yield row

def scan_dwt(infile, dwtfile):
    """
    Scan a FASTA file with a DWT model and return the best match per sequence
    
    Parameters
    ----------
    infile : str
        inputfile in FASTA format

    dwtfile : str
        motif in DWT format
    """
    # Convert FASTA to right format
    tmpfa = motevo_fa(infile)
    seqs = [">" + seq for seq in Fasta(tmpfa).ids]

    # Run the prediction
    tmpout = NamedTemporaryFile(delete=False)
    cmd = "{} -i {} -d {} -o {} -c 0 -b"
    p = sp.Popen(cmd.format(dwt_pred, tmpfa, dwtfile, tmpout.name),
            shell=True, stdout=sp.PIPE, stderr=sp.PIPE)
    p.communicate()
    
    # Filter out one match with highest score (post prob)
    max_score = {}
    match = {}
    tmpout.readline()
    for line in tmpout:
        vals = line.strip().split("\t")
        score = float(vals[7])
        if score >= max_score.get(vals[0], 0):
            max_score[vals[0]] = score
            match[vals[0]] = vals

    motif_name = os.path.basename(dwtfile).replace(".dwt", "")
    # Yield results
    p = re.compile(">?(\w+):(\d+)-(\d+)")
    for seq in seqs:
        vals = match.get(seq, [seq, 0, 0, "+", motif_name, "", 0, 0])
        chrom = seq
        mstart = int(vals[1])
        mend = int(vals[2])
        strand = vals[3]
        name = vals[4]
        score = float(vals[7])
        
        m = p.search(seq)
        if m:
            chrom = m.group(1)
            start = int(m.group(2))
            mstart = mstart + start 
            mend = mend + start
        
        yield chrom, mstart, mend, name, score, strand
    os.unlink(tmpfa)
    tmpout.close()

if __name__ == "__main__":
    from glob import glob
    import time
    #predict_dwt("bla.fa", "p53_motif.pwm", "p53.dwt")
    #for row in scan_dwt(Fasta("small.fa"), "p53.dwt"):
    
    #    print "{}\t{}\t{}\t{}\t{}\t{}".format(*row)
    print time.strftime("%H:%M:%S")
    for row in scan_fa_with_dwts("randu.fa", ["../src/data/dwt/CREB1/HepG2.CREB1.GimmeMotifs_1.dwt"]):
        #print row
        print "{}\t{}\t{}\t{}\t{}\t{}".format(*row)
        pass
    #print time.strftime("%H:%M:%S")
    #for row in scan_fa_with_dwts("test500.fa", ["../src/data/dwt/CREB1/HepG2.CREB1.GimmeMotifs_1.dwt"], score="prob"):
    #    #print row
    #    print "{}\t{}\t{}\t{}\t{}\t{}".format(*row)
    #    pass
    #print time.strftime("%H:%M:%S")



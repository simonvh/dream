import numpy as np
import sys
import gzip
WINSIZE = 200

func = np.mean
if not sys.argv[2] in ["mean", "max"]:
    print "mean or max"
    sys.exit(1)
if sys.argv[2] == "max":
    func = np.max

WINBIN = int(sys.argv[3])

def read_vals(f):
    line = f.readline()
    if not line:
        return None
    vals = line.strip().split("\t")
    vals[1], vals[2] = int(vals[1]), int(vals[2])
    vals[3] = float(vals[3])
    return vals

lines = []
if sys.argv[1].endswith(".gz"):
    f = gzip.open(sys.argv[1])
else:
    f = open(sys.argv[1])

for i in range(WINBIN - 1):
    lines.append(read_vals(f))


for i in range(WINBIN / 2):
    print "{}\t{}\t{}\t{}".format(*lines[i])

vals = read_vals(f)
while vals:
    lines.append(vals)
    chrom,start,end = lines[WINBIN / 2][:3]
    eval_vals = [lines[WINBIN/2][3]]
    for i in range(WINBIN / 2):
        if lines[i][0] == chrom and lines[i][2] > start - WINSIZE:
            eval_vals.append(lines[i][3])

    for i in range(WINBIN / 2 + 1, WINBIN):
        if lines[i][0] == chrom and lines[i][1] < end + WINSIZE:
            eval_vals.append(lines[i][3])

    
    print "{}\t{}\t{}\t{}".format(chrom, start, end, func(eval_vals))
    lines.pop(0)
    vals = read_vals(f)

for i in range(WINBIN / 2 + 1, WINBIN):
    print "{}\t{}\t{}\t{}".format(*lines[i - 1])

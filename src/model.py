from sklearn.externals import joblib 
import pandas as pd
import sys
import numpy as np
import re

#factor = sys.argv[1]
#batch = sys.argv[2]

def predict_proba(factor, fname):

    clf = joblib.load('{0}/model/{0}.model.pkl'.format(factor))

    df = pd.read_table(fname, index_col=0)
    
    cols = [x.strip().split("\t")[0] for x in open("{}/features.txt".format(factor)).readlines()]
    
    df = df[cols]

    df["proba"] = clf.predict_proba(df)[:,1]

    return df
    #df["proba"].to_csv("{}/{}.proba.txt".format(factor, batch), sep="\t")

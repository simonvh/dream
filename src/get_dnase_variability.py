#!/usr/bin/env python
import os
import sys
import glob
from multiprocessing import Pool
from functools import partial
import pandas as pd
from fluff.track import BigWigTrack

sys.path.append("src")
from utils import read_config


config = sys.argv[1]
conf = read_config(config)

bw_dir = conf['DNASE_WIG'].format(celltype="*")
bw_files = glob.glob(bw_dir)


fnames = glob.glob(conf["SPLIT_DIR"] + "/x[a-z][a-z]")
fnames = [f for f in fnames if not os.path.exists(f + ".dnasevar.bed")]

tracks = {}
for bw_file in bw_files:
    celltype = os.path.split(bw_file)[-1].split(".")[1]
    tracks[celltype] = bw_file


def get_stats(fname):
    #print fname
    df = pd.read_table(fname, usecols=[0,1,2], 
            names=["chrom", "start", "end"])
    for celltype, bw_file in tracks.items():
        track = BigWigTrack(bw_file)
        it = track.binned_stats(fname, nbins=1, statistic="max")
        vals = [row[-1] for row in it]
        df[celltype] = vals
    df["var"] = df.iloc[:,3:].std(1) / df.iloc[:,3:].mean(1)
    return df[["chrom", "start", "end", "var"]].fillna(0) 


p = Pool(8)

#for fname in fnames:
#    df = get_stats(fname)
#    print df

for i, df in enumerate(p.imap(get_stats, fnames)):
    fname = fnames[i]
    print fname
    df[["chrom", "start", "end", "var"]].to_csv(fname + ".dnasevar.bed",
            sep="\t", header=False, index=False)

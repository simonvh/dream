import os
import urllib
import pandas as pd
from glob import glob

ANNOTATION = "ftp://ftp.sanger.ac.uk/pub/gencode/Gencode_human/release_19/gencode.v19.annotation.gtf.gz"
OUTDIR = "data/expression"
DREAM = "../"

fname = os.path.join(OUTDIR, os.path.split(ANNOTATION)[-1])

if not os.path.exists(fname):
    urllib.urlretrieve(ANNOTATION, fname)

df = pd.read_table(fname, comment="#",
        names=["chrom", "source", "feature", "start",
            "end", "score", "strand", "frame", "attribute"]
        )

# Select only genes
df = df[df["feature"] == "gene"]
# Select the TSS
df["tss"] = 0
df.loc[df["strand"] == "+","tss"] = df[df["strand"] == "+"]["start"]
df.loc[df["strand"] == "-","tss"] = df[df["strand"] == "-"]["end"]
# Set name to ENSEMBL ID
df.loc[:,"name"] = df["attribute"].str.replace('.+(ENSG\d+\.\d+).+', '\\1')

df_tss = df[["chrom", "tss", "name", "strand"]]
df_tss = df_tss.sort_values(["chrom", "tss"])

# Load the DREAM expression tables
fnames = glob(os.path.join(DREAM, "RNAseq/gene_expression*tsv"))
celltypes = {}
for fname in fnames:
    celltype, rep = os.path.basename(fname).split(".")[1:3]
    df_exp = pd.read_table(fname, index_col=0)[["TPM"]]
    # Solve inconsistency (bit of a hack)
    if celltype == "IMR90":
        celltype = "IMR-90"
    basename = '.'.join([celltype, rep])
    celltypes[celltype] = 1
    df_exp.columns = [[basename]]
    df_tss = df_tss.join(df_exp, on="name")
                        
# Write output BED files
for celltype in celltypes.keys():
    cols = [c for c in df_tss.columns if c.startswith(celltype)]
    tmp_df = df_tss[["chrom", "tss", "name", "strand"]]
    tmp_df.loc[:,"score"] = df_tss[cols].mean(1)
    out_name = os.path.join(OUTDIR, "expression.{}.tss.bed.gz".format(celltype))
    tmp_df = tmp_df[["chrom", "tss", "tss", "name", "score", "strand"]]
    tmp_df.to_csv(out_name, sep="\t", compression="gzip", header=False, index=False)

cols = df_tss.columns[5:]
tmp_df = df_tss[["chrom", "tss", "name", "strand"]]
tmp_df.loc[:,"score"] = df_tss[cols].std(1)
out_name = os.path.join(OUTDIR, "expression.{}.tss.bed.gz".format("std"))
tmp_df = tmp_df[["chrom", "tss", "tss", "name", "score", "strand"]]
tmp_df.to_csv(out_name, sep="\t", compression="gzip", header=False, index=False)

from glob import glob
import gzip
import logging
import os
import random
import shutil
import tempfile
import hashlib

from gimmemotifs.config import MotifConfig
from gimmemotifs.utils import as_fasta
from gimmemotifs.motif import read_motifs
from gimmemotifs.scanner import Scanner
import gimmemotifs.background as bg
import pybedtools
import pandas as pd
from scipy.stats import scoreatpercentile
import numpy as np

from features import get_nucleotide_features, get_fractionbed_features, get_dnase_features,get_motif_features, get_fos_features,get_expression_features,get_fp_features,get_dwt_features,get_dnasebam_features,get_dnasevar_features
from utils import reservoir_sampling

logger = logging.getLogger()

def get_train_labels(conf, tmpdir,  n=100, overwrite=False):

    outdir = conf["OUTDIR"]
    factor = conf["FACTOR"]

    train_file = os.path.join(outdir, "train_labels.bed")
    if overwrite or not os.path.exists(train_file):
        for fname in glob(os.path.join(outdir, "features*")):
            os.unlink(fname)
        
        logger.info("creating training set")
        
        labelbase = conf["LABELS"]
        labels = labelbase.format(factor=factor)
        logger.debug("labels: {}".format(labels))
       
        fo = gzip.GzipFile(labels)
        if "TRAIN_CELL" in conf:
            print conf["TRAIN_CELL"]
            celltypes = conf["TRAIN_CELL"].split(",")
            print celltypes
        else:
            celltypes = fo.readline().strip().split("\t")[3:]
            celltypes = [c for c in celltypes if c != "SK-N-SH"]
    
        nexp = len(celltypes) 
        logger.debug("%s celltypes (%s)", nexp, ",".join(celltypes))

        peakbase = conf["PEAKS"]        
        nlines = n / nexp
        
        out = tempfile.NamedTemporaryFile()
        logger.debug("sampling positive regions")

        for i,celltype in enumerate(celltypes):
            peaks = peakbase.format(celltype=celltype, factor=factor)
            
            peakbed = pybedtools.BedTool(peaks)
            npeaks = peakbed.count()
            it = peakbed
            # If number of peaks is higher than required number
            # of features, sample 
            if nlines < npeaks:
                it = pybedtools.BedTool(peaks).sample(n=nlines)
            for f in it:
                center = (f.start + f.end) / 2
                out.write("{}\t{}\t{}\t{}\t{}\n".format(
                   f.chrom, center - 100, center + 100, celltype, 1))
            
            
        logger.debug("sampling negative regions")
        it = reservoir_sampling(fo, celltypes, int(nlines * 0.66))
        for chrom, start, end, celltype in it:
            out.write("{}\t{}\t{}\t{}\t{}\n".format(
                chrom, start, end, celltype, 0))
        
        logger.debug("sampling negative regions (positive in other celltype)")
        it = reservoir_sampling(fo, celltypes, int(nlines * 0.33), with_B=True)
        for chrom, start, end, celltype in it:
            out.write("{}\t{}\t{}\t{}\t{}\n".format(
                chrom, start, end, celltype, 0))

        out.flush() 
        name = os.path.join(tmpdir, os.path.basename(train_file))
        pybedtools.BedTool(out.name).sort().saveas(name)
        shutil.copy(name, train_file)
    else:
       logger.info("copying train_file")
       shutil.copy(train_file, tmpdir)

    return os.path.join(tmpdir, os.path.basename(train_file)) 

def prepare_motifs(conf, index_dir):
    logger.debug("preparing motif files")
    outdir = conf["OUTDIR"]
    factor = conf["FACTOR"]

    motifs = {}
    motifs["count"] = read_motifs(open(conf["MOTIFS_COUNT"]))
    motifs["score"] = read_motifs(open(conf["MOTIFS_SCORE"]))
    motifs["fp"] = []
    
    for fname in glob(conf["MOTIFS_FACTOR"].format(factor=factor)):
        for n in ["count", "score"]:
            motifs[n] += read_motifs(open(fname))

    for fname in glob(conf["MOTIFS_FP"].format(factor=factor)):
        motifs["fp"] += read_motifs(open(fname))
    
    fnames = {}
    for n in ["count", "score", "fp"]:
        fname = os.path.join(outdir, "{}.{}.pwm".format(factor, n))
        with open(fname, "w") as f:
            for m in motifs[n]:
                f.write("{}\n".format(m.to_pwm()))
        fnames[n] = fname
    
    fa = os.path.join(outdir, "random.w200.fa")
    if not os.path.exists(fa):
        logger.debug("generating random sequences")
        m = bg.RandomGenomicFasta(index_dir, 200, 10000)
        m.writefasta(fa)

    fdr = 0.01
    fname = os.path.join(outdir, "threshold.{}.txt".format(fdr))
    if not os.path.exists(fname):
        logger.debug("getting motif scanning threshold")
        motifs = read_motifs(open(fnames["count"]))
        s = Scanner()
        s.set_motifs(fnames["count"])

        score_table = []
        for scores in s.best_score(fa):
            score_table.append(scores)

        with open(fname, "w") as f:
            for i,scores in enumerate(np.array(score_table).transpose()):
                motif = motifs[i]
                pwm = motif.pwm
                min_score = motif.pwm_min_score()
                if len(scores) > 0:
                    opt_score = scoreatpercentile(scores, 100 - (100 * fdr))
                    cutoff = (opt_score - min_score) / (
                                motif.pwm_max_score() - min_score)
                    f.write("{}\t{}\t{}\n".format(                                                     motif.id, opt_score , cutoff))
    fnames["threshold"] = fname        
    return fnames

def make_features(infile, conf, pwms, outdir, features=None):

    # Get gimme index dir
    genome = conf["GENOME"]
    config = MotifConfig()
    index_dir = os.path.join(config.get_index_dir(), genome)
    # input in FASTA format
    fa = as_fasta(infile, index_dir)
    
    # CpG islands
    cpgfile = conf["CPG"]

    all_features = {
        "nuc": [get_nucleotide_features, 
                (fa,)],
        "cpg": [get_fractionbed_features, 
                (infile, cpgfile)],
        "dnase": [get_dnase_features,
                (infile, conf)],
        "dnase2": [get_dnase_features,
                (infile, conf, 5, 400)],
        "dnasebam": [get_dnasebam_features,
                (infile, conf, 3, 200)],
        "dnasebam2": [get_dnasebam_features,
                (infile, conf, 10, 150)],
        "dnasevar": [get_dnasevar_features,
                (infile, conf)],
        "dwt": [get_dwt_features,
                (fa, conf)],
        "motif.score": [get_motif_features,
                (fa, pwms["score"])],
        "motif.count": [get_motif_features,
                (fa, pwms["count"], True, pwms["threshold"])],
        "fos": [get_fos_features,
                (fa, conf, pwms["fp"])],
        "fp": [get_fp_features,
                (fa, conf, pwms["fp"])],
        "expression": [get_expression_features,
                (infile, conf)],
    }
  
    if features and len(features) > 1:
        features = dict((k, all_features[k]) for k in features)
    else:
        features = all_features

    # Extract the features
    dfs = []
    # Shuffle the order, just so that concurrent jobs will not 
    # try to access the disk all at the same time. Kind of.
    f_items = features.items()
    random.shuffle(f_items)
    for n, (f,args) in f_items:
 
        fname = os.path.join(outdir, "features.{}.txt.gz".format(n))
        if os.path.exists(fname):
            logger.info("loading existing features: %s", n)
            df = pd.read_table(fname, index_col=0)
        else:
            logger.info("getting features: %s", n)
            df = f(*args, name=n)
            df.to_csv(fname, sep="\t", compression="gzip")
        dfs.append(df)
    
    # Big DataFrame with all features
    df = pd.concat(dfs, 1)
    
    return df


def make_train_features(conf, tmpdir, outdir, features=None):
    # Create the motif-specific pwm files
    genome = conf["GENOME"]
    config = MotifConfig()
    index_dir = os.path.join(config.get_index_dir(), genome)
    default_pwm = os.path.join(config.get_motif_dir(), 
        config.get_default_params()["motif_db"])
    pwms = prepare_motifs(conf, index_dir)
    
    # Get the regions with accompanying labels
    ntrain = int(conf["NTRAIN"])
    train_labels = get_train_labels(conf, tmpdir, n=ntrain)
    logger.debug("training set: %s", train_labels)
    
    # Get all the features
    df = make_features(train_labels, conf, pwms, outdir, features)
       
    # Remove all features with more than 95% 0's
    size = df.shape[0]
    nonzero = (df != 0).sum(0)
    df = df[list(nonzero[nonzero > (size * 0.05)].index)]
    
    # Save features
    fname = os.path.join(outdir, "features.all.txt.gz")
    df.to_csv(fname, sep="\t", compression="gzip")
    
    # Save labels
    df = pd.read_table(train_labels, usecols=[4], names=["y"])
    fname = os.path.join(outdir, "y.txt.gz")
    df.to_csv(fname, compression="gzip", index=False, header=False)
    
def make_test_features(infile, conf, tmpdir, outdir, featurefile=None):
    factor = conf['FACTOR']
    pwms = {}
    for n in ["fp", "count", "score"]:
        pwms[n] = os.path.join(outdir, "{}.{}.pwm".format(factor, n))
    pwms["threshold"] = os.path.join(outdir, "threshold.0.01.txt")
    
    features = None
    rename = []
    if featurefile:
        features = {}
        for line in open(featurefile):
            f,name = line.strip().split("\t")
            features[name.split("_")[0]] = 1
            rename.append([f,name])

    md5 = hashlib.md5(open(infile).read()).hexdigest()
    tmpdir = os.path.join(tmpdir, md5)
    if not os.path.exists(tmpdir):
        os.makedirs(tmpdir)

    fname = os.path.join(tmpdir, "infile.bed")
    cell_type = conf["PREDICT_CELL"]
    with open(fname, "w") as out:
        for line in open(infile):
            vals = line.strip().split("\t")
            out.write("\t".join(vals + [cell_type]) + "\n")
    df = make_features(fname, conf, pwms, tmpdir, features=features) 
    df = df[[x[1] for x in rename]]
    df.columns = [x[0] for x in rename]
    return df

import pandas as pd 
import numpy as np
from gimmemotifs.motif import read_motifs
from sklearn.ensemble import RandomForestClassifier,GradientBoostingClassifier,VotingClassifier
from sklearn.cross_validation import StratifiedKFold
from sklearn.metrics import roc_auc_score
from sklearn.preprocessing import MinMaxScaler
from sklearn.linear_model import LogisticRegressionCV
from lightning.classification import CDClassifier
from sklearn.externals import joblib
from sklearn.pipeline import Pipeline
from sklearn.feature_selection import SelectFromModel
import xgboost
import sys
import os

factor = sys.argv[1]

# Motifs
motif_file_count = os.path.join(factor, "{}.count.pwm".format(factor))
motif_file_score = os.path.join(factor, "{}.score.pwm".format(factor))

# Load features
print "reading data"
X = pd.read_table("{}/features.all.txt.gz".format(factor), index_col=0)
y = np.array(pd.read_table("{}/y.txt.gz".format(factor), names=['y'])['y'])

# Model definition
r = RandomForestClassifier(n_estimators=1000, criterion="entropy", n_jobs=-1)
g = GradientBoostingClassifier(loss='deviance', learning_rate=0.1, n_estimators=300,max_depth=4)
c = CDClassifier(
        penalty="l1/l2",
        loss="log",
        multiclass=False,
        max_iter=20,
        alpha=1e-3,
        C=1.0/X.shape[0],
        tol=1e-3)
lr = LogisticRegressionCV(Cs=30, n_jobs=-1)

xgb = xgboost.XGBClassifier(
        n_estimators=150,
        learning_rate=0.1,
        max_depth=5,
        min_child_weight= 2,
        objective='binary:logistic'
        )

# Ensemble
model = VotingClassifier(
        estimators =[
            ("rf",r), 
            ("cdc",c), 
            ("g", g), 
            ("lr", lr),
            ("xgb", xgb),
            ], 
        voting='soft')

print "feature selection"
fsel = SelectFromModel(
            RandomForestClassifier(
                n_estimators=750, 
                criterion="entropy", 
                n_jobs=-1))

fsel.fit(X,y)
features = X.columns[fsel.get_support()]

# Save the name of the selected features
with open("{}/features.txt".format(factor), "w") as out:
    for i,f in enumerate(features):
        out.write("f{}\t{}\n".format(i,f))

X = X[features]
X.columns = ["f{}".format(i) for i in range(X.shape[1])]
print "fitting model"
model.fit(X, y)
print "done"

# Write a file with all motifs selected by feature selection
# on basis of *score*
with open("{}/motifs.pwm".format(factor), "w") as out:
    for m in read_motifs(open(motif_file_score)):
        if m.id + "_score" in features:
            out.write("{}\n".format(m.to_pwm()))

# Write a file with all motifs selected by feature selection
# on basis of *count*
with open("{}/count_motifs.pwm".format(factor), "w") as out:
    for m in read_motifs(open(motif_file_count)):
        if m.id + "_count" in features:
            out.write("{}\n".format(m.to_pwm()))

# Save the model
if not os.path.exists("{0}/model".format(factor)):
    os.makedirs("{0}/model".format(factor))

joblib.dump(model, "{0}/model/{0}.model.pkl".format(factor))





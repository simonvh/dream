from fluff.track import BigWigTrack
from gimmemotifs.scanner import Scanner
from gimmemotifs.config import MotifConfig
from gimmemotifs.utils import as_fasta
from gimmemotifs.motif import read_motifs
from pybedtools import BedTool
from pyDNase import BAMHandler,GenomicInterval
from tempfile import NamedTemporaryFile
from sklearn.preprocessing import MinMaxScaler
import pandas as pd
import numpy as np
import argparse
import sys
import os 
import re
import glob
from configobj import ConfigObj

def read_config(fname):
    conf = dict(ConfigObj(fname))
    p = re.compile(r'\$(({\w+}|\w+))')
    replace = 1
    while replace:
        replace = 0
        for k,v in conf.items():
            m = p.search(v)
            if m:
                replace = 1
                conf[k] = v.replace("${}".format(m.group(1)),
                        conf[m.group(1).strip("{}")])
    return conf

NUCS = ["A","C","G","T"]
def generate_kmers(n=3, x=[]):
    if len(x) == 0:
        for k in generate_kmers(n, NUCS):
            yield k
    elif len(x[0]) == n:
        for k in x:
            yield k
    else:
        for nuc in NUCS:
            for k in generate_kmers(n, [y + nuc for y in x]):
                yield k

EXTEND = 200
NBINS = 11
DINUCS = [n for n in generate_kmers(2)]
#TRINUCS = [n for n in generate_kmers(3)]
TRINUCS = ["CCG", "GCG", "CGC", "CGG"]

def dnase(bedfile, bwfile, genome, nbins=NBINS, extend=EXTEND, norm=False):

    in_bed = BedTool(bedfile)
    in_dnase = BigWigTrack(bwfile)

    norm_mean = 0.0
    norm_sd = 1.0
    if norm and os.path.exists("norm_factor.txt"):
        print "normalizing DNAse"
        df_norm = pd.read_table("norm_factor.txt", index_col=0)
        norm_mean = df_norm.loc[os.path.basename(bwfile), "mean"]
        norm_sd = df_norm.loc[os.path.basename(bwfile), "std"]

    b2 = in_bed.slop(b=extend, g=genome)
    
    tmp = NamedTemporaryFile()
    b2.saveas(tmp.name)
    
    yield (
        ["dnase_max_{}_{}_{}".format(extend, nbins, i) for i in range(nbins)] +
        ["log2_dnase_max_{}_{}_{}".format(extend, nbins, i) for i in range(nbins)]
)
    
    for r in in_dnase.binned_stats(tmp.name, nbins, statistic="max"):
        r_log = (np.log2(np.array(r[3:]) + 0.001) - norm_mean) / norm_sd
        yield np.hstack((r[3:],r_log))
        #yield (r_log - norm_mean) / norm_sd
#
def fos_scores(fa, pwmfile, bamfiles):

    bams = [BAMHandler(b) for b in bamfiles]
    s = Scanner()
    s.set_motifs(pwmfile)
    motifs = read_motifs(open(pwmfile))
    result_it = s.scan(fa, 1, True, 0)
    p = re.compile(r'([^\s:]+):(\d+)-(\d+)')
    fos = {}
    smap = {1:"+", -1:"-"}
    for m in motifs:
        fos[m.id] = []
    for i, result in enumerate(result_it):
        seq_id = fa.ids[i]
        m = p.search(seq_id)
        chrom, start, end = m.group(1), int(m.group(2)), int(m.group(3))
        for motif, matches in zip(motifs, result):

            (score, pos, strand) = matches[0]
            g = GenomicInterval(chrom,
                    start + pos,
                    start + pos + len(motif),
                    strand=smap[strand])
            f = np.mean([b.FOS(g) for b in bams])
            fos[motif.id].append(f)
    df = pd.DataFrame()
    for k,v in fos.items():
        df["{}_fos".format(k)] = v

    return df


def intersect_cpg(infile, cpgfile):
    cpg_bed = BedTool(cpgfile)
    in_bed = BedTool(infile)
    overlap = np.array([float(f[-1]) for f in in_bed.coverage(cpg_bed)])
    ret_df = pd.DataFrame()
    ret_df["cpg_overlap"] = overlap
    return ret_df

def intersect_fp(fa, fp_file, pwmfile):

    s = Scanner()
    s.set_motifs(pwmfile)
    motifs = read_motifs(open(pwmfile))   

    result_it = s.scan(fa, 1, True, 0)
    p = re.compile(r'([^\s:]+):(\d+)-(\d+)')
    
    tmpfile = {}
    for m in motifs:
        tmpfile[m.id] = NamedTemporaryFile(delete=False)
    
    for i, result in enumerate(result_it):
        seq_id = fa.ids[i]
        m = p.search(seq_id)
        chrom, start, end = m.group(1), int(m.group(2)), int(m.group(3))
        for motif, matches in zip(motifs, result):
            (score, pos, strand) = matches[0]
            tmpfile[motif.id].write("{}\t{}\t{}\t{}:{}-{}\n".format(
                chrom, start + pos, start + pos + len(motif),
		chrom, start, end
		))
    
    for m in motifs:
        tmpfile[m.id].flush()
  
    fp_bed = BedTool(fp_file)
    ret_df = pd.DataFrame()
    for m in motifs:
        in_bed = BedTool(tmpfile[m.id].name)
        overlap = np.array([float(f[-1]) for f in in_bed.coverage(fp_bed)])
        ret_df["{}_fp_overlap".format(m.id)] = overlap
        score = np.array([float(f[-1]) for f in in_bed.map(fp_bed, o="max", null=0)])
        ret_df["{}_fp_n".format(m.id)] = score
        
    return ret_df

def nuc(fa):

    yield NUCS + DINUCS + TRINUCS + ["AT%", "GC%"]
    for seq in fa.seqs:
        l = float(len(seq))
        freq = [seq.upper().count(x) / l for x in NUCS + DINUCS + TRINUCS]
        yield freq + [freq[0] + freq[3]] + [freq[1] + freq[2]]
        

def closest_exp(infile, exp_file):

    in_bed = BedTool(infile)
    exp_bed = BedTool(exp_file)
    a =  np.array([[int(f[-1]), float(f[-3]) ] for f in in_bed.closest(exp_bed, D="b", t='first')])

    ret_df = pd.DataFrame(a, columns=["dist_sqrt", "expression"])
    ret_df["dist_sqrt"] = np.sqrt(np.abs(ret_df["dist_sqrt"])) * np.sign(ret_df["dist_sqrt"])
    ret_df["log2_expression"] = np.log2(ret_df["expression"] + 1)
    ret_df["exp_dist_sqrt"] = ret_df["expression"] / (np.abs(ret_df["dist_sqrt"]) + 1)
    ret_df["exp_dist"] = ret_df["expression"] / (ret_df["dist_sqrt"] ** 2 + 1)

    return ret_df

def nuc(fa):

    yield NUCS + DINUCS + TRINUCS + ["AT%", "GC%"]
    for seq in fa.seqs:
        l = float(len(seq))
        freq = [seq.upper().count(x) / l for x in NUCS + DINUCS + TRINUCS]
        yield freq + [freq[0] + freq[3]] + [freq[1] + freq[2]]



def scan_motifs(fa, pwmfile, count=False, genome="hg19", cutoff=None):
    
    s = Scanner()
    s.set_motifs(pwmfile)

    motifs = read_motifs(open(pwmfile))   
    score_min = np.array([m.pwm_min_score() for m in motifs])
    score_range = np.array([m.pwm_max_score() -  m.pwm_min_score() for m in motifs])
    
    if count:
        result_it = s.count(fa, nreport=100, scan_rc=True, cutoff=cutoff)
    else:
        result_it = s.best_score(fa, scan_rc=True)
    
    # header
    if count:
        yield [m.id + "_count" for m in motifs]
    else:
        yield [m.id + "_score" for m in motifs]
    # score table
    for i,scores in enumerate(result_it):
        if count:
            yield scores
        else:
            yield (np.array(scores) - score_min) / score_range

def scan_motifs_with_fp(fa, pwmfile, fp_file, count=False, genome="hg19", cutoff=None):

    s = Scanner()
    s.set_motifs(pwmfile)

    motifs = read_motifs(open(pwmfile))   
    score_min = np.array([m.pwm_min_score() for m in motifs])
    score_range = np.array([m.pwm_max_score() -  m.pwm_min_score() for m in motifs])
    
    result_it = s.scan(fa, 1, scan_rc=True, cutoff=0)
    for matches in result_it:
        pass

genome = "hg19"
config = MotifConfig()
DEFAULT_PWM = os.path.join(config.get_motif_dir(), 
        config.get_default_params()["motif_db"])
index_dir = os.path.join(config.get_index_dir(), genome)

p = argparse.ArgumentParser()
p.add_argument("config",
    help="config file",
    metavar="CFG")
p.add_argument("infile",
    help="inputfile (BED format)",
    metavar="INFILE")
p.add_argument("outfile",
    help="outputfile (TSV format)",
    metavar="OUTFILE")
p.add_argument("bwfile",
    help="DNAse bigWig file",
    metavar="BWFILE")
p.add_argument("celltype",
    help="cell type",
    metavar="CELLTYPE")
p.add_argument("-p", "--pwmfile",
    dest="pwmfile",
    help="pwmfile with motifs",
    metavar="PWMFILE",
    default=DEFAULT_PWM)
p.add_argument("-q", "--pwmfile2",
    dest="pwmfile_count",
    help="pwmfile with motifs for counts",
    metavar="PWMFILE",
    default=None)
p.add_argument("-f", "--pwmfile_fp",
    dest="pwmfile_fp",
    help="pwmfile with motifs for footprints",
    metavar="PWMFILE",
    default=None)
p.add_argument("-c", "--cutoff",
    dest="cutoff",
    help="threshold file",
    metavar="CUTOFF",
    default=None)
p.add_argument("-e", "--extend",
    dest="extend",
    help="extend sequences",
    metavar="INT",
    type=int,
    default=None)

args = p.parse_args()   
celltype = args.celltype
conf = read_config(args.config)

genome_size = conf["GENOMESIZE"]

# CpG islands
cpgfile = conf["CPG"]

# BAM files of DNAse data
dnase_bam = conf["DNASE_BAM"]
bamfiles = glob.glob("{}/*{}*bam".format(dnase_bam, celltype))

# BED file with footprints
fp_dir = conf["FP_DIR"]
fp_file = "{}/{}.fp.bed.gz".format(fp_dir, celltype)

# Expression 
exp_dir = conf["EXP_DIR"]
exp_file = "{}/expression.{}.tss.bed.gz".format(exp_dir, celltype)

infile = args.infile
outfile = args.outfile
pwmfile = args.pwmfile
pwmfile_count = args.pwmfile_count
pwmfile_fp = args.pwmfile_fp
cutoff = args.cutoff

if args.extend:
    in_bed = BedTool(infile)
    b2 = in_bed.slop(b=args.extend, g=genome_size)
    tmp = NamedTemporaryFile()
    b2.saveas(tmp.name)
    fa = as_fasta(tmp.name, index_dir)
else:
    fa = as_fasta(infile, index_dir)

bwfile = args.bwfile

a_fp = intersect_fp(fa, fp_file, pwmfile_fp) 
a_exp = closest_exp(infile, exp_file)
a_cpg = intersect_cpg(infile, cpgfile)
#a_fos = fos_scores(fa, pwmfile_fp, bamfiles)

a_motifs = [row for row in scan_motifs(fa, pwmfile, genome=genome)]
a_motifs = pd.DataFrame(a_motifs[1:], columns=a_motifs[0])

if pwmfile_count:
    a_mcount = [row for row in scan_motifs(fa, pwmfile_count, 
        count=True, genome=genome, cutoff=cutoff)]

a_dnase = [row for row in dnase(infile, bwfile, genome_size)]
a_dnase2 = [row for row in dnase(infile, bwfile, genome_size, nbins=5, extend=400, norm=True)]
a_nuc = [row for row in nuc(fa)]

combine = [
    a_motifs,
    a_fp,
    #a_fos,
    a_exp,
    a_cpg,
    pd.DataFrame(a_dnase[1:], columns=a_dnase[0]),
    pd.DataFrame(a_dnase2[1:], columns=a_dnase2[0]),
    pd.DataFrame(a_nuc[1:], columns=a_nuc[0])
]
if pwmfile_count:
    combine.append(
        pd.DataFrame(a_mcount[1:], columns=a_mcount[0])
    )

pd.concat(combine, 1).to_csv(outfile, sep="\t")

from configobj import ConfigObj
import re

def read_config(fname):
    conf = dict(ConfigObj(fname))
    p = re.compile(r'\$(({\w+}|\w+))')
    replace = 1
    while replace:
        replace = 0
        for k,v in conf.items():
            m = p.search(v)
            if m:
                replace = 1
                conf[k] = v.replace("${}".format(m.group(1)), 
                        conf[m.group(1).strip("{}")])
    return conf

from configobj import ConfigObj
import random
import re
import logging

def read_config(fname):
    conf = dict(ConfigObj(fname))
    p = re.compile(r'\$(({\w+}|\w+))')
    replace = 1
    while replace:
        replace = 0
        for k,v in conf.items():
            m = p.search(v)
            if m:
                replace = 1
                conf[k] = v.replace("${}".format(m.group(1)),
                        conf[m.group(1).strip("{}")])
    return conf

def reservoir_sampling(fo, celltypes, N, with_B=False):
    """ 
    Select `N` unbound regions for every celltype from file 
    object `fo`, based on reservoir sampling.

    Parameters
    ----------

    fo : file object
    
    celltypes : list
        list of celltypes

    N : int
        number of regions to select

    Yields
    ------
    tuple
        tuple of (chrom, start, end, celltype)
    """

    sample = dict([(j,[]) for j in celltypes])
    c = dict([(j,0) for j in celltypes])
    for line in fo:
        vals = line.strip().split("\t")
        if not with_B or "B" in vals or len(vals) == 4:
            for i,j in enumerate(celltypes):
                if vals[i + 3] == "U":
                    if c[j] < N:
                        sample[j].append(vals[:3])
                    elif c[j] >= N and random.random() < N/float(c[j]+1):
                        replace = random.randint(0,N-1)
                        sample[j][replace] = vals[:3]
                    c[j] += 1
 
    for k,v in sample.items():
        for row in v:
            yield row + [k]

def setup_logging():
    """
    Setup logging to stdout and return logger object.

    Returns
    -------

    logger
    """
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    # create formatter
    formatter = logging.Formatter(
            '%(asctime)s %(levelname)s: %(message)s', datefmt='%H:%M:%S')

    # add formatter to ch
    ch.setFormatter(formatter)

    # add ch to logger
    logger.addHandler(ch)

    return logger

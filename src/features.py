from fluff.track import BigWigTrack,BamTrack
from gimmemotifs.scanner import Scanner
from gimmemotifs.config import MotifConfig
from gimmemotifs.utils import as_fasta
from gimmemotifs.motif import read_motifs
from pybedtools import BedTool
from pyDNase import BAMHandler,GenomicInterval
from tempfile import NamedTemporaryFile
from sklearn.preprocessing import MinMaxScaler
from dwt import scan_fa_with_dwts
import pandas as pd
import numpy as np
import argparse
import sys
import os 
import re
import glob
from configobj import ConfigObj
import logging

logger = logging.getLogger()

NUCS = ["A","C","G","T"]
NBINS = 11
EXTEND = 200

def generate_kmers(n=3, x=[]):
    """ Generate all k-mers of length `n`

    Parameters
    ----------

    n : int
        length of k-mers

    Returns
    -------

    list
        list with all k-mers of length `n`
    """

    if len(x) == 0:
        for k in generate_kmers(n, NUCS):
            yield k
    elif len(x[0]) == n:
        for k in x:
            yield k
    else:
        for nuc in NUCS:
            for k in generate_kmers(n, [y + nuc for y in x]):
                yield k

def get_nucleotide_features(fa, name="nuc"):
    """ 
    Calculate nucleotide frequency-based features:
 
    * single nucleotide frequencies
    * di-nucleotide frequences
    * tri-nucleotide frequences (only "CCG", "GCG", "CGC", "CGG")
    * AT% and GC%

    Parameters
    ----------

    fa : Fasta
        Fasta object with input sequences

    Returns
    -------

    DataFrame
        pandas DataFrame with features
    """

    DINUCS = [n for n in generate_kmers(2)]
    #TRINUCS = [n for n in generate_kmers(3)]
    TRINUCS = ["CCG", "GCG", "CGC", "CGG"]

    a = []
    for seq in fa.seqs:
        l = float(len(seq))
        freq = [seq.upper().count(x) / l for x in NUCS + DINUCS + TRINUCS]
        a.append(freq + [freq[0] + freq[3]] + [freq[1] + freq[2]])
    
    columns = NUCS + DINUCS + TRINUCS + ["AT%", "GC%"]
    columns = ["{}_{}".format(name, c) for c in columns]
    df = pd.DataFrame(a, index=fa.ids, columns=columns)
    return df    

def index_from_bed(infile):
    # Get the index
    df = pd.read_table(infile, usecols=[0,1,2,3], 
            names=["chrom", "start", "end", "value"])
    df["name"] = df["chrom"] + ":" + df["start"].astype(str) + "-" + \
                    df["end"].astype(str) + " " +df["value"].astype(str)
    return list(df["name"])
 
def get_fractionbed_features(infile, bedfile, name="bed"):
    """
    Calculate fraction overlap with a BED file

    Parameters
    ----------

    infile, bedfile : str
        input files in BED format

    Returns
    -------

    DataFrame
        pandas DataFrame with features
    """

    other_bed = BedTool(bedfile)
    in_bed = BedTool(infile)

    # Calculate overlap (bedtools coverage)
    overlap = np.array([float(f[-1]) for f in in_bed.coverage(other_bed)])
   
    # Create the dataframe
    columns = ["fraction_overlap"]
    columns = ["{}_{}".format(name, c) for c in columns]
    df = pd.DataFrame(overlap, columns=columns, 
            index=index_from_bed(infile))
    
    
    return df

def get_dnasebam_features(infile, conf, nbins=NBINS, extend=EXTEND, norm=False, 
        name="dnasebam"):
    """
    Calculate number of reads for a certain number of windows (DNaseI for now)

    Parameters
    ----------

    infile : str
        input file in BED format

    conf : dict
        configuration dict

    nbins : int, optional
        number of bins to use

    extend : int, optional
        extend BED features with `extend` both up- and downstream

    norm : bool, optional
        normalize signal, not implemented yet

    Returns
    -------

    DataFrame
        pandas DataFrame with features
    """
    genome = conf["GENOMESIZE"]
    in_bed = BedTool(infile)
    extended_bed = in_bed.slop(b=extend, g=genome)
    in_df = pd.read_table(infile, 
            names=["chrom", "start", "end", "celltype", "f"])
    celltypes = in_df["celltype"].unique()
   
    # DataFrame with features
    df = pd.DataFrame(index=index_from_bed(infile))
   
    for i in range(nbins):
        df["dnase_log2_count_{}_{}_{}".format(extend, nbins, i)] = 0
    
    for celltype in celltypes:
        bamfiles = glob.glob(conf["DNASE_BAM"].format(celltype=celltype))
        if len(bamfiles) == 0:
            logger.warn("No DNaseI bams for %s, skipping features", celltype)
            continue
        
        # Write temporary BED file
        tmp = NamedTemporaryFile()
        for f in extended_bed:
            if f[3] == celltype:
                tmp.write(str(f))
        tmp.flush()
 
        dfs = []
        for bamfile in bamfiles:
            track = BamTrack(bamfile) 
       
            a = []
            for r in track.binned_stats(tmp.name, nbins, split=True, statistic="count"):
                a.append(np.log2(np.array(r[3:]) + 1)) 
            
            a = np.array(a)

            df.loc[list(in_df["celltype"] == celltype), :] += a
    
    df.columns = ["{}_{}".format(name, c) for c in df.columns]
    
    # Scale between 0 and 1
    for col in df.columns:
        df[col] = MinMaxScaler().fit_transform(df[[col]])
    
    return df

def get_dnasevar_features(infile, conf, 
        name="dnasevar"):
    """
    Get DNAseI variability

    Parameters
    ----------

    infile : str
        input file in BED format

    conf : dict
        configuration dict

    Returns
    -------

    DataFrame
        pandas DataFrame with features
    """

    fname = os.path.join(conf["BASE"], "data", "dnase.variability.bed.gz")
    dnase_bed = BedTool(fname)
    in_bed = BedTool(infile)
    
    # Make an intermediate BED file
    # This is relatively quick and will save a lot of memory
    intersection = dnase_bed.intersect(in_bed, wa=True)

    vals = [float(f[-1]) for f in in_bed.map(intersection, f=0.5, c=4, o="mean")] 

    df = pd.DataFrame(
            {"dnase_variability": vals}, 
            index=index_from_bed(infile)
            )
    
    df.columns = ["{}_{}".format(name, c) for c in df.columns]
    return df


def get_dnase_features(infile, conf, nbins=NBINS, extend=EXTEND, norm=False, 
        name="dnase"):
    """
    Calculate bigWig-based features (DNaseI for now)

    Parameters
    ----------

    infile : str
        input file in BED format

    conf : dict
        configuration dict

    nbins : int, optional
        number of bins to use

    extend : int, optional
        extend BED features with `extend` both up- and downstream

    norm : bool, optional
        normalize signal, not implemented yet

    Returns
    -------

    DataFrame
        pandas DataFrame with features
    """

    genome = conf["GENOMESIZE"]
    in_bed = BedTool(infile)
    extended_bed = in_bed.slop(b=extend, g=genome)
    in_df = pd.read_table(infile, 
            names=["chrom", "start", "end", "celltype", "f"])
    celltypes = in_df["celltype"].unique()
   
    # DataFrame with features
    df = pd.DataFrame(index=index_from_bed(infile))
   
    for i in range(nbins):
        df["dnase_max_{}_{}_{}".format(extend, nbins, i)] = 0
    for i in range(nbins):
        df["log2_dnase_max_{}_{}_{}".format(extend, nbins, i)] = 0
    
    norm_mean = 0
    norm_sd = 1
 
#    if norm and os.path.exists("norm_factor.txt"):
#        print "normalizing DNAse"
#        df_norm = pd.read_table("norm_factor.txt", index_col=0)
#        norm_mean = df_norm.loc[os.path.basename(bwfile), "mean"]
#        norm_sd = df_norm.loc[os.path.basename(bwfile), "std"]
    
 
    for celltype in celltypes:
        bigwigs = glob.glob(
                conf["DNASE_WIG"].format(celltype=celltype))
        if len(bigwigs) == 0:
            logger.warn("No DNaseI bigWigs for %s, skipping features", celltype)
            continue
        elif len(bigwigs) > 1:
            logger.warn("Can't use multiple DNaseI bigWigs for %s, skipping features", celltype)
            continue

        bwfile= bigwigs[0]
        in_dnase = BigWigTrack(bwfile)
    
        tmp = NamedTemporaryFile()
        for f in extended_bed:
            if f[3] == celltype:
                tmp.write(str(f))
        tmp.flush()
    
        
        a = []
        for r in in_dnase.binned_stats(tmp.name, nbins, statistic="max"):
            r_log = (np.log2(np.array(r[3:]) + 0.001) - norm_mean) / norm_sd
            a.append(np.hstack((r[3:],r_log)))

        df.loc[list(in_df["celltype"] == celltype), :] = a
    
    df.columns = ["{}_{}".format(name, c) for c in df.columns]
    
    return df

def get_dwt_features(fa, conf, name="dwt"):
    """
    Calculate features based on DWT motif scanning. Returns 
    score of best motif (posterior probability)
    
    Parameters
    ----------

    fa : Fasta
        input as Fasta object

    conf : dict
        conf object

    Returns
    -------

    DataFrame
        pandas DataFrame with features
    """

    factor = conf["FACTOR"]
    dwts = glob.glob(conf["DWT"].format(factor=factor))
    
    scores = [r for r in scan_fa_with_dwts(fa, dwts, scores_only=True)]
    values = np.reshape(scores, [len(scores) / len(dwts), len(dwts)], 1)
    columns = [os.path.basename(x) for x in dwts]

    df = pd.DataFrame(values, index=fa.ids, columns=columns)
    df.columns = ["{}_{}".format(name, c) for c in df.columns]
    return df


def get_motif_features(fa, pwmfile, count=False, cutoff=None, name="motif"):
    """
    Calculate features based on motif scanning. By default
    return score of best motif. By providing `count` as True
    it will return the number of matches.

    Parameters
    ----------

    fa : Fasta
        input as Fasta object

    pwmfile : str
        filename of pwm motif file

    count : bool, optional
        if `count` is True, don't return best motif score

    cutoff : optional
        cutoffs for count-based motif scanning

    Returns
    -------

    DataFrame
        pandas DataFrame with features
    """

    s = Scanner()
    s.set_motifs(pwmfile)

    motifs = read_motifs(open(pwmfile))   
    score_min = np.array([m.pwm_min_score() for m in motifs])
    score_range = np.array(
            [m.pwm_max_score() - m.pwm_min_score() for m in motifs])
    
    if count:
        result_it = s.count(fa, nreport=100, scan_rc=True, cutoff=cutoff)
    else:
        result_it = s.best_score(fa, scan_rc=True)
    
    # header
    if count:
        columns = [m.id + "_count" for m in motifs]
    else:
        columns = [m.id + "_score" for m in motifs]
    
    # score/counts
    values = []
    for i,scores in enumerate(result_it):
        if count:
            values.append(scores)
        else:
            values.append((np.array(scores) - score_min) / score_range)

    df = pd.DataFrame(values, index=fa.ids, columns=columns)
    if not count:
        for col in df.columns:
            df[col] = MinMaxScaler().fit_transform(df[[col]])
    
    df.columns = ["{}_{}".format(name, c) for c in df.columns]
    return df

def get_fos_features(fa, conf, pwmfile, name="fos"):
    
    idx = fa.ids
    celltypes = [x.split(" ")[-1] for x in idx]
   
    bams = {}
    for celltype in celltypes:
        if not celltype in bams:
            bamfiles = glob.glob(conf["DNASE_BAM"].format(celltype=celltype))
            bams[celltype] = [BAMHandler(b) for b in bamfiles]
    
    s = Scanner()
    s.set_motifs(pwmfile)
    motifs = read_motifs(open(pwmfile))
    
    # DataFrame with features
    df = pd.DataFrame(index=idx)
    
    for motif in motifs:
        df["{}_fos".format(motif.id)] = 0
    
    result_it = s.scan(fa, 1, True, 0)
    p = re.compile(r'([^\s:]+):(\d+)-(\d+)')
    fos = {}
    smap = {1:"+", -1:"-"}
    for m in motifs:
        fos[m.id] = []

    values = []
    for seq_id, celltype, result in zip(idx, celltypes, result_it):
        m = p.search(seq_id)
        chrom, start, end = m.group(1), int(m.group(2)), int(m.group(3))
        row = []
        for motif, matches in zip(motifs, result):

            (score, pos, strand) = matches[0]
            g = GenomicInterval(chrom,
                    start + pos,
                    start + pos + len(motif),
                    strand=smap[strand])
            f = np.mean([b.FOS(g) for b in bams[celltype]])
            row.append(f)
        values.append(row)
    
    df.loc[:,:] = values
    df = df.fillna(2) 
    df.columns = ["{}_{}".format(name, c) for c in df.columns]
    return df

def get_expression_features(infile, conf, name="expr"):
    
    
    in_df = pd.read_table(infile, 
            names=["chrom", "start", "end", "celltype", "f"])
    celltypes = in_df["celltype"].unique()
   
    # DataFrame with features
    df = pd.DataFrame(index=index_from_bed(infile))
  
    columns = ["dist", "abs_dist", "dist_sqrt", "abs_dist_sqrt", 
            "expression", "log2_expression", "log2_exp_dist_sqrt", "log2_exp_dist"]
    for col in columns:
        df[col] = 0

    in_bed = BedTool(infile)
    for celltype in celltypes: 
        tmp = NamedTemporaryFile()
        for f in in_bed:
            if f[3] == celltype:
                tmp.write(str(f))
        tmp.flush()
    
        exp_file = conf["EXPRESSION"].format(celltype=celltype)
        exp_bed = BedTool(exp_file)
    
        a = np.array([[int(f[-1]), float(f[-3]) ] for f in in_bed.closest(exp_bed, D="b", t='first')])

        ret_df = pd.DataFrame(a, columns=["dist", "expression"])
        ret_df["abs_dist"] = np.abs(ret_df["dist"])
        ret_df["dist_sqrt"] = np.sqrt(np.abs(ret_df["dist"])) * np.sign(ret_df["dist"])
        ret_df["abs_dist_sqrt"] = np.abs(ret_df["dist_sqrt"])
        ret_df["log2_expression"] = np.log2(ret_df["expression"] + 1)
        ret_df["log2_exp_dist_sqrt"] = ret_df["log2_expression"] / (np.abs(ret_df["dist_sqrt"]) + 1)
        ret_df["log2_exp_dist"] = ret_df["log2_expression"] / (ret_df["dist_sqrt"] ** 2 + 1)
        df.loc[list(in_df["celltype"] == celltype), :] = ret_df[columns].values
    exp_file = conf["EXPRESSION"].format(celltype="std")
    exp_bed = BedTool(exp_file)
    a = np.array([float(f[-3]) for f in in_bed.closest(exp_bed, D="b", t='first')])
    df["std_cell_types"] = a 
    
    for col in ["abs_dist","abs_dist_sqrt","expression", 
            "log2_expression","log2_exp_dist_sqrt","log2_exp_dist"]:
        df[col] = MinMaxScaler().fit_transform(df[[col]])
    
    df.columns = ["{}_{}".format(name, c) for c in df.columns]
    return df

def get_fp_features(fa, conf, pwmfile, name="fp"):

    s = Scanner()
    s.set_motifs(pwmfile)
    motifs = read_motifs(open(pwmfile))   

    result_it = s.scan(fa, 1, True, 0)
    p = re.compile(r'([^\s:]+):(\d+)-(\d+)\s+(.+)')
    
    tmpfile = {}
    for m in motifs:
        tmpfile[m.id] = NamedTemporaryFile(delete=False)
    
    celltypes = []
    for i, result in enumerate(result_it):
        seq_id = fa.ids[i]
        m = p.search(seq_id)
        chrom, start, end = m.group(1), int(m.group(2)), int(m.group(3))
        celltype = m.group(4)
        celltypes.append(celltype)
        for motif, matches in zip(motifs, result):
            (score, pos, strand) = matches[0]
            tmpfile[motif.id].write("{}\t{}\t{}\t{}\n".format(
                chrom, start + pos, start + pos + len(motif), celltype
		))
    
    for m in motifs:
        tmpfile[m.id].flush()
  
    df = pd.DataFrame({"celltype":celltypes}, index=fa.ids) 
    for m in motifs:
        df["{}_fp_overlap".format(m.id)] = 0
        df["{}_fp_n".format(m.id)] = 0
    
        in_bed = BedTool(tmpfile[m.id].name)

        for celltype in df["celltype"].unique(): 
            tmp = NamedTemporaryFile()
            for f in in_bed:
                if f[3] == celltype:
                    tmp.write(str(f))
            tmp.flush()
            tmp_bed = BedTool(tmp.name)

            fp_file = conf["FP"].format(celltype=celltype)
            fp_bed = BedTool(fp_file)
  
            overlap = np.array([float(f[-1]) for f in tmp_bed.coverage(fp_bed)])
            df.loc[df["celltype"] == celltype, "{}_fp_overlap".format(m.id)] = overlap
            score = np.array([float(f[-1]) for f in tmp_bed.map(fp_bed, o="max", null=0)])
            df.loc[df["celltype"] == celltype, "{}_fp_n".format(m.id)] = score
    
    df = df[[c for c in df.columns if c != "celltype"]]
    df.columns = ["{}_{}".format(name, c) for c in df.columns]
    
    return df

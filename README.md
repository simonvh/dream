# GimmeBinding - Simon van Heeringen 

## Software installation

Install a Python 2 version of miniconda. Then install dependencies:

``` 
$ conda install biofluff gimmemotifs=0.10.0b4
$ pip install pyDNase
```

## Preprocessing / intermediate data

These steps were done, and the output is in `data/`

* Predict motifs (in `data/gimme`)
* Predict footprints (in `data/fp_hint`)
* Summarize expression data (in `/data/expression`)


## Configuration

The file config.txt contains all relevant variable definitions.
Most important to change are:

* HOME
* DREAMBASE
* BASE
* FACTOR
* PREDICT_CELL
* NTRAIN

For the final conference round of the challenge NTRAIN=100000 was used.
This requires somewhere between 64GB and 256GB of memory (but no more).

Before the first run, the ladder regions need to be split in chunks (I used
100,000 sequences) using the linux split command and placed in a specific
directory. This directory is then specified in the config file.

## Run

Change all the relevant variables and run do_all.sh:

``` 
$ ./do_all.sh config.txt
```

As none of the methods was optimized for speed, 
this will likely take quite some time. 

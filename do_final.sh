#!/bin/bash

config=`realpath $1`

if [ -z $config ]; then
        echo "Usage: run_all.sh <configfile>"
        exit
elif [ ! -e $config ]; then
        echo "No such file $config"
        exit
fi

source $config

if [ ! -e $BASE/$FACTOR ]; then
    exit;
fi

cat $BASE/$FACTOR/*.$PREDICT_CELL.tab | gzip > $BASE/$FACTOR/raw.L.$FACTOR.$PREDICT_CELL.tab.gz && python $BASE/src/post_process.py $BASE/$FACTOR/raw.L.$FACTOR.$PREDICT_CELL.tab.gz max 3 | gzip > $BASE/$FACTOR/L.$FACTOR.$PREDICT_CELL.tab.gz
